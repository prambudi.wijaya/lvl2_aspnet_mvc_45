﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Transactions;

namespace LVL2_ASPNet_MVC_45.Controllers
{
    public class EmployeeController : Controller
    {
        HR14Entities1 db = new HR14Entities1();

        public ActionResult Index()
        {
            return View(db.Employees.ToList());
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(Employee employee)
        {
            try
            {
                //employee.MiddleName = Request["salary"].ToString();

                using (TransactionScope ts = new TransactionScope())
                {
                    db.Employees.Add(employee);
                    db.SaveChanges();

                    int a = 0;
                    int c = 10 / a;
                    tbl_salary sal = new tbl_salary();
                    sal.Name = employee.FirstName;
                    sal.salary = Convert.ToInt32(Request["salary"].ToString());
                    db.tbl_salary.Add(sal);
                    db.SaveChanges();

                    ts.Complete();

                }

                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                ViewData["data"] = ex.Message;
                return View();
            }
            
        }

        public ActionResult Details(int id)
        {
            using (db)
            {
                return View(db.Employees.Where(n => n.Id == id).FirstOrDefault());
            }
            
        }

        public ActionResult Edit(int id)
        {
            return View(db.Employees.Where(n => n.Id == id).FirstOrDefault());
        }
        [HttpPost]
        public ActionResult Edit(Employee employee)
        {
            try
            {
                db.Entry(employee).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Delete(int id)
        {
            return View(db.Employees.Where(n => n.Id == id).FirstOrDefault());
        }
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collect)
        {
            try
            {
                //using (TransactionScope ts = new TransactionScope())
                //{
                    
                    //int a = 0;
                    //int c = 10 / a;
                    
                    Employee e = db.Employees.Where(n => n.Id == id).FirstOrDefault();
                    db.Employees.Remove(e);
                    db.SaveChanges();
                    return RedirectToAction("Index");

                    //ts.Complete();
                //}
                    
            }
            catch(Exception ex)
            {
                ViewData["data"] = ex.Message;
                return View();
            }
        }
    }
}